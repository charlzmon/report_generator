#!/bin/bash

echo Executing single report generator with organisation:$1, bem: $2, email:$3, start_ts:$4, end_ts:$5

#source ~/.venvs/repgen/bin/activate

cd single_battery_usage
python single_battery_usage_report.py "$1" "$2" "$3" "$4" "$5"
