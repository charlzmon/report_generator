#Import libraries

import click

import pandas as pd
from pymongo import MongoClient
from datetime import datetime, timedelta, timezone
import time
import math


import numpy as np
import pdfkit
from jinja2 import Environment, FileSystemLoader

from matplotlib.ticker import MaxNLocator
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker

import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders



#Mongo definitions

MdbURI = 'mongodb://balancell:7104Test@ec2-34-245-39-222.eu-west-1.compute.amazonaws.com:27017/balancell_dev'
client = MongoClient(MdbURI)
db = client['balancell_dev']

assets_collection = db['assets']
org_collection = db['organisations']
bemConfig_collection = db['bemConfigs']
orgBEM_collection = db['organisationBems']
buckets_collection = db['minuteEnergyGraphBuckets']
cell_collection = db['cellTypes']

FMT = '%Y-%m-%d %H:%M'



#Function declarations

def rate_calc(df,state):
    try:
        workrate = df.loc[df['newstate'] == state, 'kWh'].sum() / ((df['newstate'].value_counts()[state])/60)
    except:
        print('No ' + str(state) + ' state for this time frame')
        workrate = 0
    return workrate

def date_range(start, end, intv):
    start = datetime.strptime(start,FMT)
    end = datetime.strptime(end,FMT)
    diff = (end  - start ) / intv
    for i in range(intv):
        yield (start + diff * i).strftime('%H:%M')
    yield end.strftime('%H:%M')
    
def new_label(ticks_loc):
    labels = []
    for tic in ticks_loc:
        time = str(int(tic)) + ':00'
        labels.append(time)
    return labels

def printm(message, val):
    print(message + ': ' + str(val))
    
def save_img(sv_fig, figure_count):
    img_name = 'media/figure_' + str(figure_count) + '.svg'
    sv_fig.tight_layout()
    sv_fig.savefig(img_name,bbox_inches='tight')
    
def usage_plotter(uid, ax, plot_idx, df, tmp_df, dates, start_hr, end_hr):
    idle_color = 'whitesmoke'
    charge_color = 'chartreuse'  #'greenyellow'   #61CE70   chartreuse
    in_use_color = 'lightskyblue'#'skyblue'  #dodgerblue   steelblue
    font_color = 'dimgray'

    date = dates[plot_idx]

    state_change_df = tmp_df[tmp_df.index.date == date].copy()
    #Shift state column by one and save comparison to new df
    state_change_df['shifted'] = tmp_df[uid] != tmp_df[uid].shift()
    #The following steps aim to add the last possible entry back into the df as it will be lost with shift()
    last_dt_str = df.index[-1] #Find last date entry in df
    last_state = df[uid][-1] #Find last state entry in df
    new_row = pd.Series({uid:last_state,'shifted':True},name=last_dt_str) #Create pd.series from data

    state_change_df = state_change_df.append(new_row) #Append series to df

    #Extract only rows for which the state changed (i.e. rows undergoing state transition)
    state_change_df = state_change_df[uid][state_change_df[state_change_df['shifted']==True].index]
    state_change_df = pd.DataFrame(state_change_df) #Convert series to df
    state_change_df['start'] = state_change_df.index.hour + (state_change_df.index.minute/60)
    #Add a column containing the next state's transition time (i.e. the end of this state)
    state_change_df['end'] = state_change_df.index.to_series().shift(-1)
    state_change_df['end'] = pd.to_datetime(state_change_df['end']) #Convert 'end' column to datetime
    #Add a column containing the time difference between start and end in hours
    state_change_df['time_diff'] = (state_change_df['end']-state_change_df.index).astype('timedelta64[m]')/60

    #Create a list of values for each state
    idle = state_change_df[state_change_df[uid]=='Idle']
    idle = idle.reset_index()[['start', 'time_diff']].values.tolist()

    inuse = state_change_df[state_change_df[uid]=='In Use']
    inuse = inuse.reset_index()[['start', 'time_diff']].values.tolist()

    charging = state_change_df[state_change_df[uid]=='Charging']
    charging = charging.reset_index()[['start', 'time_diff']].values.tolist()

    #Overlay three horizontal bar charts on subplot
    ax.broken_barh(idle, [0, .1], facecolor=idle_color)
    ax.broken_barh(inuse, [0, .1], facecolor=in_use_color)
    ax.broken_barh(charging, [0, .1], facecolor=charge_color)

    #X-axis tick label formatting
    label_format = '{}'   
    n_range = end_hr - start_hr

    ax.xaxis.set_major_locator(MaxNLocator(n_range, min_n_ticks=n_range)) #Sets the nr of tics to number of hours
    ticks_loc = [*range(start_hr, end_hr, 1)]#ax1.get_xticks().tolist() #Returns list of tic locations
    labels = new_label(ticks_loc) #Creates formatted tic labels
    ax.xaxis.set_major_locator(mticker.FixedLocator(ticks_loc)) #Sets tic locations
    ax.set_xticklabels([label_format.format(x) for x in labels]) #Updates tic labels with new labels
    ax.set_xlim([start_hr,end_hr])
    ax.axes.get_xaxis().set_visible(True)
    ax.axes.get_yaxis().set_visible(False)

    #Plot and axis changes
    ax.set_facecolor(idle_color)
    ax.axes.get_yaxis().set_visible(False)
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.spines['left'].set_visible(False)
    ax.tick_params(axis='x', colors=font_color, rotation=70)
    ax.set_title(datetime.strftime(date, '%d %B %Y')) #, color=font_color

def figure_generator(uid, dates, num_dates_to_plot_on_this_fig, first_date_index, df, tmp_df, start_hr, end_hr):
    figwidth = 12
    rowheight = 2
    figr, axs = plt.subplots(nrows=num_dates_to_plot_on_this_fig, 
                             ncols=1, 
                             figsize=(figwidth,rowheight * num_dates_to_plot_on_this_fig), 
                             sharey=True)
    plt.xlim(start_hr, end_hr)
    plt.ylim(0, 0.2)
    
    if(num_dates_to_plot_on_this_fig > 1): #If this fig has only one date to plot, the enumerate function in next line will result in error
        for n, ax in enumerate(axs.flatten()):
            plt_idx = n + first_date_index
            usage_plotter(uid, ax, plt_idx, df, tmp_df, dates, start_hr, end_hr)
    else:
        usage_plotter(uid, axs, first_date_index, df, tmp_df, dates, start_hr, end_hr)
        
        
    return figr

def add_zeros_to_pie_df(pi_df):
    try:
        pi_df['Charging']
    except:
        pi_df.loc['Charging'] = 0
        
    try:
        pi_df['In Use']
    except:
        pi_df.loc['In Use'] = 0
        
    try:
        pi_df['Idle']
    except:
        pi_df.loc['Idle'] = 0
        
    return pi_df

def clickPrint(str):
    click.echo(str)
    click.echo()

@click.command()
@click.argument('org_id')
@click.argument('bem_id')
@click.argument('email')
@click.argument('start_ts_ms')
@click.argument('end_ts_ms')
def deloader(org_id, bem_id, email, start_ts_ms, end_ts_ms):



    #Initialise vars and dicts

    empty_batteries = 0
    bem_config = list(bemConfig_collection.find({'_id':bem_id}))[0]
    org = list(org_collection.find({'_id':org_id}))[0]
    
    organisation_name = org['name']

    bem = {}

    try:
        org_branding = org['branding']
    except:
        with open('./../balancell_logo_b64_ext.txt') as f:
            org_branding = f.readlines()
        org_branding = org_branding[0]
    if(org_branding==''):
            with open('./../balancell_logo_b64_ext.txt') as f:
                org_branding = f.readlines()
            org_branding = org_branding[0]

    try:
        asset = list(assets_collection.find({'bem':bem_config['_id']}))[0]
    except:
        asset = {'_id':'','name':'No asset specified','serial':'','type':'','city':'','region':'','organisation':org_id,'bem':bem_config['_id'],'submitted':0.0}

    uid = bem_config['bemID']
    cell_nominal_voltage = 3.25
    cell_count = int(bem_config['numCells'])
    cell_capacity = int(bem_config['capacity'])
    battery_voltage = cell_nominal_voltage * cell_count
    Whr = cell_capacity * battery_voltage
    battery_capacity = round(Whr/1000,2)

    bem[uid] = {'organisation':organisation_name,
                'org_branding':org_branding,
                
                'bem_name':bem_config['name'],
                'battery_capacity':battery_capacity, 
                'shunt_direction':bem_config['shuntDirection'],
                'battery_Ah':cell_capacity,
                'battery_voltage':battery_voltage,
                
                'asset_name':asset['name'],
                'asset_serial':asset['serial'],
                'asset_type':asset['type'], 
                'asset_city':asset['city'],
                'asset_region':asset['region'],
                
                }



    #Processing scripts

    #Datetime conversions
    start_ts_ms = int(start_ts_ms)
    end_ts_ms = int(end_ts_ms)

    start_dt = datetime.fromtimestamp(start_ts_ms/1000)
    end_dt = datetime.fromtimestamp(end_ts_ms/1000)

    start_date = datetime.strftime(start_dt.date(), '%d %b %Y')
    end_date = datetime.strftime(end_dt, '%d %b %Y')

    start_ts_ms = start_ts_ms*1.0
    end_ts_ms = end_ts_ms*1.0
    #####
    
    delta = (end_dt-start_dt) / timedelta(minutes=1) #Save the difference and calculate minutes between dates
    diff = int(delta)

    column_names = ['date', 'connectivity']
    bem_df = pd.DataFrame(columns = column_names)
    bem_df['date'] = (pd.date_range(start=start_dt, periods=diff,freq='min')) #Each 'date' entry is a date that is incremented by a single minute
    bem_df['date'] = bem_df['date'].map(lambda t: t.strftime('%Y-%m-%d %H:%M')) #Strip seconds from date
    bem_df.set_index('date',inplace=True,drop=True) #Set date as index

    print(str(start_dt) + ': ' + str(start_ts_ms))
    print(str(end_dt) + ': ' + str(end_ts_ms))
    print()

    #Query Mongo for this UID's minutebuckets between specified dates
    buckets = list(buckets_collection.find({'bemUID':uid,'bucketStart':{'$gte':start_ts_ms,'$lte':end_ts_ms }},
                        {'bemDate': 1, 'soc': 1, 'batteryState': 1, 'energyIn': 1, 'energyOut': 1, 'energyDelta': 1, 'cycleCounter.default.counter': 1}).sort('bucketStart', 1))
    df = pd.DataFrame(buckets)

    if df.empty:
        print("Empty df\n")
        empty_batteries += 1
    else:
        try:
            df['batteryState'] 
        except:
            err_msg = 'No battery state recorded in minutebuckets'
            print(err_msg)
            #continue

        df['cycleCounter'] = pd.json_normalize(df.cycleCounter)

        min_cycle_counter = min(df['cycleCounter'])
        max_cycle_counter = max(df['cycleCounter'])

        bem[uid]['min_cycle_counter'] = min_cycle_counter
        bem[uid]['max_cycle_counter'] = max_cycle_counter

        E_max = bem[uid]['battery_capacity'] * 3600 *1000 #Calculate battery capacity in joules
        soc_b = df['soc'].iloc[0] # first element --> soc at beginning of window
        soc_f = df['soc'].iloc[-1] # last element --> soc at end of window

        #Shunt direction switches which column represents energyin and energyout
        if(bem[uid]['shunt_direction'] == 1):
            e_in = df['energyIn'].iloc[-1] - df['energyIn'].iloc[0]
            e_out = df['energyOut'].iloc[-1] - df['energyOut'].iloc[0]
        else:
            e_out = df['energyIn'].iloc[-1] - df['energyIn'].iloc[0]
            e_in = df['energyOut'].iloc[-1] - df['energyOut'].iloc[0]       

        soc_diff = soc_b-soc_f
        efficiency = (e_out - (soc_diff*E_max))/ e_in
        if(efficiency <= 1):
            bem[uid]['efficiency'] = efficiency
        elif(np.isnan(efficiency)):
            print('Efficiency is ' + str(efficiency) + ', \n e_in = ' + str(e_in) + '\n')
            efficiency = 1
        else:
            print('Efficiency calculation error! Efficienccy is higher than 100%: ' + str(efficiency*100) + '\n')

        #Remove duplicate and NAN values, set time as index
        df.drop('_id', inplace=True, axis=1)
        df = df[df['soc'].notna()] #Drop NAN values from buckets df
        df.set_index('bemDate', inplace=True, drop=True) #Set date as index
        df.index = pd.to_datetime(df.index) #Convert index to datetime 
        df.index =df.index.map(lambda t: t.strftime('%Y-%m-%d %H:%M')) #Strip seconds from dates
        df = df[~df.index.duplicated()] #Remove duplicate entries

        #Create state of charge rate of change column: Difference in SOC from one minute to next
        df['socroc'] = df['soc'].pct_change(1) 

        #Remove all NAN values from new column
        df = df[df['socroc'].notna()] 
        df = df.replace(np.NINF, np.NAN)
        df = df.replace(np.inf, np.NAN)
        df.dropna(inplace=True)

        threshold = 0.0009 #Battery at rest(idle) will not have a soc roc larger than |0.0009|
        charging_conditional = df['batteryState'] == 'Charging' #All buckets in charging mode
        not_charging_conditional = df['batteryState'] != 'Charging' #All buckets not in charging mode (in use or idle)
        inside_thresh = (df['socroc'] > -threshold) & (df['socroc'] < threshold) #All buckets in idle
        lt_thresh = df['socroc'] <= -threshold #All buckets in use
        gt_thresh = df['socroc'] >= threshold #All buckets in charge

        df.loc[lt_thresh & not_charging_conditional, 'batteryState'] = 'In Use'
        df.loc[gt_thresh | charging_conditional, 'batteryState'] = 'Charging'
        df.loc[inside_thresh, 'batteryState'] = 'Idle'

        df['newstate'] = ''
        newstate_idx = df.columns.get_loc('newstate')

        bucket_list = df[['batteryState','newstate']].values.tolist()
        list_len = len(bucket_list)
        #The following functionality could not be accomplished using pandas vectorisation, so it is converted to list
        for i in range(list_len):
            
            from_end = list_len-i
            #This loop tries to catch "idle" states nestled between "in use" states. Only 5 sequential "idle"...
            #... states will result in "idle". Otherwise t
            #From_end ensures that index out of range is not referenced in subsequent condition checks
            #The first if condition also checks whether newState has already been assigned an "idle" state...
            #...this ensures the same buckets are not revisited each iteration of the for loop
            if( (from_end>1) and (bucket_list[i][0] == 'Idle') and (bucket_list[i][1] != 'Idle') ):
                if((from_end>2) and (bucket_list[i+1][0] == 'Idle')):
                    if((from_end>3) and (bucket_list[i+2][0] == 'Idle')):
                        if((from_end>4) and (bucket_list[i+3][0] == 'Idle')):
                            if((from_end>5) and (bucket_list[i+4][0] == 'Idle')):
                                if((from_end>6) and (bucket_list[i+5][0] == 'Idle')):
                                    bucket_list[i][1] = 'Idle'
                                    bucket_list[i+1][1] = 'Idle'
                                    bucket_list[i+2][1] = 'Idle'
                                    bucket_list[i+3][1] = 'Idle'
                                    bucket_list[i+4][1] = 'Idle'
                                else:
                                    bucket_list[i][1] = 'In Use'
                                    bucket_list[i+1][1] = 'In Use'
                                    bucket_list[i+2][1] = 'In Use'
                                    bucket_list[i+3][1] = 'In Use'
                                    bucket_list[i+4][1] = 'In Use'
                                    #print( bucket_list[i+4])
                            else:
                                bucket_list[i][1] = 'In Use'
                                bucket_list[i+1][1] = 'In Use'
                                bucket_list[i+2][1] = 'In Use'
                                bucket_list[i+3][1] = 'In Use'
                        else:
                            bucket_list[i][1] = 'In Use'
                            bucket_list[i+1][1] = 'In Use'
                            bucket_list[i+2][1] = 'In Use'
                    else:
                        bucket_list[i][1] = 'In Use'
                        bucket_list[i+1][1] = 'In Use'
                else:
                    bucket_list[i][1] = 'In Use'

        battery_state_series = pd.Series( (v[0] for v in bucket_list) )
        battery_state_series.index = df.index

        new_state_series = pd.Series( (v[1] for v in bucket_list) )
        new_state_series.index = df.index

        df['batteryState'] = battery_state_series
        df['newstate'] = new_state_series

        use_idx = battery_state_series == 'In Use'   #df['batteryState'] #################################################Shouldn't this come from new state?
        chg_idx = battery_state_series == 'Charging'    #df['batteryState']
        df.loc[use_idx, 'newstate'] = 'In Use'
        df.loc[chg_idx, 'newstate'] = 'Charging'
        


        df['kJ'] = (df['energyDelta'] - df['energyDelta'].shift())/1000 #Total energy used in the minute
        df['kW'] = df['kJ']/60 #Energy per second -->  60 seconds in a minute
        df['kWh'] = df['kW']/60 #Basically multiply by the hours the work was done for which is one minute (1/60)

        work_rate = rate_calc(df,'In Use')
        work_rate += rate_calc(df,'Idle')
        charge_rate = rate_calc(df,'Charging')

        bem[uid]['workrate'] = work_rate*bem[uid]['shunt_direction']
        bem[uid]['charge_rate'] = charge_rate
        

        new_df = df.drop(df.columns.difference(['newstate','soc']), inplace=True, axis=1)
        new_df = df.replace(np.NINF, np.NAN)
        new_df = df.replace(np.inf, np.NAN)
        new_df.dropna(inplace=True)
        bem_df[uid] = new_df['newstate']

    bem_df.drop('connectivity',inplace=True,axis=1)
    df = bem_df
    df.dropna(inplace=True)
    df.drop(df[uid][[-1]].index,inplace=True)
    df.drop(df.tail(5).index,inplace=True) #Drop the last 5 rows from the df

    #Convert to SA time
    #datetime.now().astimezone().tzinfo
    df.index = pd.to_datetime(df.index)
    df.index = df.index.tz_localize(tz='UTC')
    df.index = df.index.tz_convert('Africa/Johannesburg')
    df.index = df.index.tz_localize(None)



    #Calculate NAN fraction

    col = df[uid]
    #Calculate fraction of entries in this column that are NAN
    nan_fraction = int(((col.count()- col.isna().sum())/col.count())*100) 
    print(uid + str(' fraction of data available: ' + str(nan_fraction) + '%'))
    if(nan_fraction < 50):
        df.drop(uid, inplace=True, axis=1)
        print(uid + ' has no or less than 50% of data present \n')

    new = (col.value_counts()/60).astype(int)
    for state in ['Idle','In Use','Charging']:
        try:
            new[state]
        except:
            print('No ' + state + ' state present \n')



    #Generate horizontal bar charts

    df.index = pd.to_datetime(df.index)

    idle_color = 'whitesmoke'
    charge_color = 'chartreuse'  #'greenyellow'   #61CE70   chartreuse
    in_use_color = 'lightskyblue'#'skyblue'  #dodgerblue   steelblue
    font_color = 'dimgray'

    dates = [] #List to hold unique dates in this timeframe
    day_diff = end_dt - start_dt #Number of unique dates in this timeframe
    min_time = max(df.index.time) #(initialised to largest possible value) Holds the earliest time for which the state is not in Idle 
    max_time = min(df.index.time) #(initialised to smallest possible value) Holds the latest time for which the state is not in Idle 

    cur_min = min_time
    cur_max = max_time

    state_count = len(df[uid].value_counts())

    for i in range(day_diff.days + 1):
        
        unique_date = (start_dt + timedelta(days=i)).date()
        dates.append(unique_date) #Add to unique date list

        mask = df[df.index.date == unique_date] #All rows which are indexed on current date
        try: #Sometimes a day has no non-Idle states (Idle all day), so the mask indexing results in undeclared variable later on
            cur_min = min(mask[mask[uid] != 'Idle'].index.time) #Earliest TIME for which the battery is not in Idle on this date
            cur_max = max(mask[mask[uid] != 'Idle'].index.time) #Latest TIME for which the battery is not in Idle on this date
        except:
            if(state_count <= 1):
                cur_min = min(df.index.time)
                cur_max = max(df.index.time)
            else:
                None

        
        if(cur_min <= min_time): #If this date's earliest non-Idle time is earlier than global minimum ,replace
            min_time = cur_min
        if(cur_max >= max_time): #If this date's latest non-Idle time is later than global maximum ,replace
            max_time = cur_max

    start_hr = min_time.hour #Integer value of starting hour
    end_hr = max_time.hour + 1 #Integer value of end hour

    #Store only rows which occurred during global minimum and maximim hours
    tmp_df = df[(df.index.hour >= start_hr) & (df.index.hour < end_hr)] 

    total_dates_to_plot = len(dates)
    last_date_index = 0
    dates_left_to_plot = total_dates_to_plot

    first_page_fig_count = 6
    full_page_fig_count = 8


    if(total_dates_to_plot <= first_page_fig_count):
        fig_count = 1
    else:
        fig_count = ((total_dates_to_plot-first_page_fig_count)/full_page_fig_count) + 1
        fig_count = int(math.ceil(fig_count))

    for figure_count in range(fig_count):
        if(figure_count == 0): #If this figure is to appear on first page
            if(total_dates_to_plot <= first_page_fig_count): #If all the figures will fit on the first page
                usage_fig = figure_generator(uid, dates, total_dates_to_plot, last_date_index, df, tmp_df, start_hr, end_hr)
                save_img(usage_fig, figure_count)
            else: #If there are more figures to plot after the first page
                usage_fig = figure_generator(uid, dates, first_page_fig_count, last_date_index, df, tmp_df, start_hr, end_hr)
                last_date_index = last_date_index + first_page_fig_count
                dates_left_to_plot = dates_left_to_plot - first_page_fig_count
                save_img(usage_fig, figure_count)
                
        elif(dates_left_to_plot > full_page_fig_count): #If there more dates left to plot than can fit on one page)
            usage_fig = figure_generator(uid, dates, full_page_fig_count, last_date_index, df, tmp_df, start_hr, end_hr)
            last_date_index = last_date_index + full_page_fig_count
            dates_left_to_plot = dates_left_to_plot - full_page_fig_count
            save_img(usage_fig, figure_count)
        else:#If on last page
            dates_left_to_plot = total_dates_to_plot - last_date_index
            usage_fig = figure_generator(uid, dates, dates_left_to_plot, last_date_index, df, tmp_df, start_hr, end_hr)
            save_img(usage_fig, figure_count)



    #Generate pie chart

    fig2 = plt.figure(figsize=(8,8))
    ax2 = fig2.add_subplot(1,1,1)

    colours = {'In Use': in_use_color,'Idle': idle_color,'Charging': charge_color}
    explode = (0.03, 0.03, 0.03)

    pie_df = (df[uid].value_counts()/60)#.astype(int)
    pie_df = pie_df[pie_df.index.isin(['In Use', 'Charging','Idle'])]
    pie_df = pie_df[(pie_df != 0)]

    pie_df = add_zeros_to_pie_df(pie_df)

    pie_labels = list(pie_df.index)
    total = sum(pie_df.values)


    ax2.pie(pie_df, labels = pie_labels , labeldistance=None, radius=1,
        autopct=lambda p: '{:.0f}'.format(p * total / 100),textprops={'color':'0','fontsize': 24},
        colors=[colours[key] for key in pie_labels],
            wedgeprops={"edgecolor":font_color,'linewidth': 1, 'linestyle': 'solid', 'antialiased': True}, 
            startangle=90,explode=explode) # shadow=True,

    for text in plt.legend().get_texts():
        text.set_color(font_color)

    if(start_date==end_date):
        rep_str = start_date
    else:
        rep_str = start_date + str('  -  ') + end_date
        
    ax2.set_title(rep_str, fontweight="bold",size=28)
    ax2.legend(title = 'in hours', prop={'size': 18}, title_fontsize='xx-large', loc = 'lower right') #'center left' lower right
    fig2.savefig('media/pie.svg',bbox_inches='tight')



    #Report
    
    customer_name_font_size = 32
    costomer_name_len = len(organisation_name)
    if(costomer_name_len > 45):
        customer_name_font_size = 22
    elif(costomer_name_len > 32):
        customer_name_font_size = 25

    env = Environment(loader=FileSystemLoader('.'))
    template = env.get_template('media/single_report_template.html')

    org_name = bem[uid]['organisation']
    org_brand = bem[uid]['org_branding']

    bem_name = bem[uid]['bem_name']
    batter_kwh = int(bem[uid]['battery_capacity'])
    battery_ah = int(bem[uid]['battery_Ah'])
    battery_voltage  = int(bem[uid]['battery_voltage'])

    workrate  = round(abs(bem[uid]['workrate']),2)
    cycle_count = int(bem[uid]['max_cycle_counter'] - bem[uid]['min_cycle_counter'])

    asset_name= bem[uid]['asset_name'].replace('(','').replace(')','')
    asset_type= bem[uid]['asset_type']
    asset_serial= bem[uid]['asset_serial']
    asset_city= bem[uid]['asset_city']
    asset_region = bem[uid]['asset_region']

    template_vars = {'org_name': org_name,
                    'org_branding': org_brand,

                    'rep_str': rep_str,

                    'bem_name': bem_name,
                    'battery_capacity': str(battery_ah) + ' Ah, ' + str(batter_kwh) + ' kWh',
                    'battery_voltage': str(battery_voltage) + 'V',

                    'workrate': str(workrate) + ' kWh/h',
                    'cycle_count':cycle_count,

                    'asset_name': asset_name ,
                    'asset_type': asset_type,
                    'asset_serial': asset_serial,
                    'asset_city': asset_city,
                    'asset_region': asset_region,
                    
                    'customer_name_font_size':customer_name_font_size,
                    'fig_count':fig_count
                    }

    html_out = template.render(template_vars) #template_vars
    with open('media/single_report.html', 'w') as f:
        f.write(html_out)

    pdfkit.from_file('media/single_report.html', 'media/single_report.pdf')



    #Email setup

    email_body = 'Good day, \nPlease find attached your requested report for ' + rep_str + '\nKind regards \nBalancell'

    sender = 'reports@balancell.co'
    password = 'R3p0rtz@r3C00l'
    cc = 'kahlkritzinger@balancell.com'
    receiver = email

    #Setup the MIME
    message = MIMEMultipart()
    message['From'] = sender
    message['To'] = receiver
    message['Cc'] = cc
    message['Subject'] = 'Report for ' + rep_str

    message.attach(MIMEText(email_body, 'plain'))

    pdfname = 'media/single_report.pdf'

    # open the file in bynary
    binary_pdf = open(pdfname, 'rb')

    payload = MIMEBase('application', 'octate-stream', Name=pdfname)
    payload.set_payload((binary_pdf).read())

    # enconding the binary into base64
    encoders.encode_base64(payload)

    # add header with pdf name
    payload.add_header('Content-Decomposition', 'attachment', filename=pdfname)
    message.attach(payload)

    #use gmail with port
    session = smtplib.SMTP('smtp.gmail.com', 587)

    #enable security
    session.starttls()

    #login with mail_id and password
    session.login(sender, password)

    text = message.as_string()
    session.sendmail(sender, receiver, text)
    session.quit()
    print('Mail Sent')



if __name__ == '__main__':
    deloader()