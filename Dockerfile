FROM python:3


ENV NODE_VERSION=10.19.0
RUN curl -sL https://deb.nodesource.com/setup_6.x | bash - \
    && apt-get install -y nodejs \
    && apt-get install -y npm
ENV NODE_ENV=production

WORKDIR /app
COPY ["package.json", "package-lock.json*", "./"]
RUN npm install --production

COPY . .

RUN pip install --no-cache-dir -r requirements.txt
RUN apt-get install -y wkhtmltopdf

EXPOSE 5000

CMD [ "node", "report_generator.js" ]
