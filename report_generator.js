const express = require("express");
const router = express.Router();
const app = express();
const exec = require('child_process').exec


port = 5000;

//Configure express to use body-parser as middle-ware.
app.use(express.json());
app.use(express.urlencoded({
    extended: true
}));

router.post('/handle', (request, response) => {
    let report_data = request.body;
    console.log(report_data);
    console.log()

    let type = report_data.type;
    let org_id = report_data.org_id;
    let bem_id = report_data.bem_id
    let email = report_data.email;
    let start_ts_ms = report_data.start;
    let end_ts_ms = report_data.end;

    if (type == 'Single Battery') {
        const single_battery_script = exec(`single_battery_usage/single_battery_usage_script.bash ${org_id} ${bem_id} ${email} ${start_ts_ms} ${end_ts_ms}`);

        single_battery_script.stdout.on('data', (data) => {
            console.log(data);
        });
        single_battery_script.stderr.on('data', (data) => {
            console.error(data);
        });
    }else if(type == 'Organisation'){
        const org_battery_script = exec(`organisation_batteries/organisation_batteries_script.bash ${org_id} ${email} ${start_ts_ms} ${end_ts_ms}`);

        org_battery_script.stdout.on('data', (data) => {
            console.log(data);

        });
        org_battery_script.stderr.on('data', (data) => {
            console.error(data);
        });   
    }

    response.end("yes");
});

// add router in the Express app.
app.use("/", router);
app.listen(5000, () => {
    console.log(`Started on PORT ${port}`);
})