import click

import pandas as pd
from pymongo import MongoClient
from datetime import datetime, timedelta
import math

import numpy as np
import pdfkit
from jinja2 import Environment, FileSystemLoader

import matplotlib.pyplot as plt

import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders

MdbURI = 'mongodb://balancell:7104Test@ec2-34-245-39-222.eu-west-1.compute.amazonaws.com:27017/balancell_dev'
client = MongoClient(MdbURI)
db = client['balancell_dev']

assets_collection = db['assets']
org_collection = db['organisations']
bemConfig_collection = db['bemConfigs']
orgBEM_collection = db['organisationBems']
buckets_collection = db['minuteEnergyGraphBuckets']
cell_collection = db['cellTypes']

#Returns a list of BEM UIDs from a given organisation name
def bems_and_assets_from_organisation(organisation_name):
    no_assets = False
    bems = {}
    org_dict = {}
    org = list(org_collection.find({'name':organisation_name}))[0]
    
    org_id = org['_id']
    
    try:
        org_branding = org['branding']
    except:
        with open('../balancell_logo_b64_ext.txt') as f:
            org_branding = f.readlines()
        org_branding = org_branding[0]
    if(org_branding==''):
        with open('../balancell_logo_b64_ext.txt') as f:
            org_branding = f.readlines()
        org_branding = org_branding[0]

    org_dict = {'organisation':organisation_name,
               'org_branding':org_branding}
        
    #If no assets found for this organisation, change the report type
    assets = list(assets_collection.find({'organisation':org_id}))
    if(assets):
        for asset in assets:
            bem_config = list(bemConfig_collection.find({'_id':asset['bem']}))[0]
    

            uid = bem_config['bemID']
            cell_nominal_voltage = 3.25
            cell_count = int(bem_config['numCells'])
            cell_capacity = int(bem_config['capacity'])
            battery_voltage = cell_nominal_voltage * cell_count
            Whr = cell_capacity * battery_voltage
            battery_capacity = round(Whr/1000,2)

            bems[bem_config['bemID']] = {'bem_name':bem_config['name'],
                                         'asset_name':asset['name'],
                                         'asset_serial':asset['serial'],
                                         'asset_type':asset['type'], 
                                         'asset_region':asset['region'],
                                         'asset_city':asset['city'],
                                         'battery_capacity':battery_capacity, 
                                         'shunt_direction':bem_config['shuntDirection'],
                                         'battery_Ah':cell_capacity,
                                         'battery_voltage':battery_voltage}
    else:
        no_assets = True
        bem_orgs = list(orgBEM_collection.find({'organisationID':org_id}))
        for bemorg in bem_orgs:
            bem_config = list(bemConfig_collection.find({'_id':bemorg['bemID']}))[0]
            
            uid = bem_config['bemID']
            cell_nominal_voltage = 3.25
            cell_count = int(bem_config['numCells'])
            cell_capacity = int(bem_config['capacity'])
            battery_voltage = cell_nominal_voltage * cell_count
            Whr = cell_capacity * battery_voltage
            battery_capacity = round(Whr/1000,2)
            bems[uid] = {'bem_name':bem_config['name'],
                                     'battery_capacity':battery_capacity, 
                                     'shunt_direction':bem_config['shuntDirection'],
                                     'battery_Ah':cell_capacity,
                                     'battery_voltage':battery_voltage}
            
    return bems,org_dict,no_assets

def rate_calc(df,state):
    try:
        workrate = df.loc[df['newstate'] == state, 'kWh'].sum() / ((df['newstate'].value_counts()[state])/60)
    except:
        print('No ' + str(state) + ' state for this time frame')
        workrate = 0
    return workrate

def duration(bems,uid,metric):
    try:
        rate = bems[uid]['battery_capacity']/abs(bems[uid][metric])
    except:
        rate = 0
    return rate

def color(state):
    color_dict = {
        'Idle' : 'whitesmoke',
        'Charging' : 'chartreuse',
        'In Use' : 'lightskyblue',
        'font' : 'dimgray',
    }
    return color_dict[state]

def make_autopct(values):
    def my_autopct(pct):
        total = sum(values)
        val = int(round(pct*total/100.0))
        return '{p:.1f}%'.format(p=pct,v=val)
    return my_autopct

def avg_trycatcher(avg_value,bem_count,round_):
    try:
        avg_value = avg_value/bem_count
    except:
        avg_value = 0
    if(round_):
        return round(avg_value,1)
    else:
        return int(avg_value)

def save_img(fig, figure_count):
    img_name = 'media/figure_' + str(figure_count) + '.svg'
    fig.savefig(img_name,bbox_inches='tight')


def plotter(fig, df, bems, rows_to_display,  last_uid_idx, no_assets):
    colours = {'In Use': color('In Use'),'Idle': color('Idle'),'Charging': color('Charging')}
    
    #last_uid_idx = last_uid_idx +1 #Keeps track of total plots thus far in order to fetch next UID
    row_idx = 0 #Loops through number of rows to display in this figure
    plt_idx = 1
    
    
    while row_idx < rows_to_display:
        uid = df.columns[last_uid_idx]
        if(no_assets):
            plt_count = 2 * rows_to_display
            ax = fig.add_subplot(plt_count,2,plt_idx,label='a' + str(plt_idx))
            bx = fig.add_subplot(plt_count,2,plt_idx+1,label='b' + str(plt_idx+1))
            plt_idx += 2

        else:
            plt_count = 3 *rows_to_display
            ax = fig.add_subplot(plt_count,3,plt_idx,label='a' + str(plt_idx))
            bx = fig.add_subplot(plt_count,3,plt_idx+1,label='b' + str(plt_idx+1))
            cx = fig.add_subplot(plt_count,3,plt_idx+2,label='c' + str(plt_idx+2))
            plt_idx += 3

        #TABLE A
        discharge_time = duration(bems,uid,'workrate')
        workrate = abs(bems[uid]['workrate'])
        cycles = int(bems[uid]['max_cycle_counter']-bems[uid]['min_cycle_counter'])

        a_arr = [['Workrate',round(workrate,1),'kWh/h'],
                     ['Cycles',cycles,''],
                     ['Use time \n(on full charge)',round(discharge_time,1),'hours']]#,

        title_a = str(int(bems[uid]['battery_voltage'])) + 'V, ' + str(bems[uid]['battery_Ah'])+ 'Ah, ' +  str(round(bems[uid]['battery_capacity'],1)) + 'kWh'
        ax.title.set_text(title_a)
        ax.axis('tight')
        ax.axis('off')
        table_a = ax.table(cellText = a_arr, loc = 'center', colWidths = [0.6, 0.3, 0.3])
        table_a.set_fontsize(40)
        table_a.scale(1, 3)            


        #PIE
        #Battery name as heading
        pie_df = (df[uid].value_counts()/60).astype(int)
        pie_df = pie_df[pie_df.index.isin(['In Use', 'Charging','Idle'])]
        pie_df = pie_df[(pie_df != 0)]
        pie_labels = list(pie_df.index)
        total = sum(pie_df.values)
        bx.pie(pie_df, labels = pie_labels , labeldistance=None, radius=1,
           autopct=lambda p: '{:.0f}'.format(p * total / 100),textprops={'color':'0','fontsize': 12},
           colors=[colours[key] for key in pie_labels],
            wedgeprops={"edgecolor":color('font'),'linewidth': 1, 'linestyle': 'solid', 'antialiased': True}, 
            startangle=90,
              )
        title_b = bems[uid]['bem_name']
        bx.title.set_text(title_b)
        
        #TABLE C
        if(no_assets==False):
            asset_type = bems[uid]['asset_type']
            asset_serial = bems[uid]['asset_serial']
            asset_region = bems[uid]['asset_region']
            asset_city = bems[uid]['asset_city']

            c_arr = [['Type',asset_type],
                         ['Serial',asset_serial],
                         ['Region',asset_region],
                         ['City',asset_city]]

            title_c = bems[uid]['asset_name']
            cx.title.set_text(title_c)
            cx.axis('tight')
            cx.axis('off')
            table_c = cx.table(cellText = c_arr, loc = 'center', colWidths = [0.3, 0.6])
            table_c.set_fontsize(40)
            table_c.scale(1, 2)

        row_idx += 1
        last_uid_idx += 1
    
    return fig

def figure_generator(df, bems, no_assets,bem_count):
    #Calculate number of figures required (one per page)
    #bem_count = len(bems)
    fig_size_dict = {}
    if(no_assets):
        row_height = 8
        fig_width = 11
        first_page_fig_count = 3
        full_page_fig_count = 4
        html_height = 200
        if(bem_count <= first_page_fig_count):
            fig_count = 1
        else:
            fig_count = ((bem_count-first_page_fig_count)/full_page_fig_count) + 1
            fig_count = int(math.ceil(fig_count))
    else:
        row_height = 10
        fig_width = 11
        first_page_fig_count = 3
        full_page_fig_count = 5
        html_height = 200
        if(bem_count <= first_page_fig_count):
            fig_count = 1
        else:
            fig_count = ((bem_count-first_page_fig_count)/full_page_fig_count) + 1
            fig_count = int(math.ceil(fig_count))
    
    rows_to_display = bem_count
    last_uid_idx = 0
    for figure_count in range(fig_count):
        if(figure_count == 0): #If this figure is to appear on first page
            if(rows_to_display <= first_page_fig_count): #If all the figures will fit on the first page
                fig = plt.figure(figsize=(fig_width,row_height*rows_to_display))
                fig = plotter(fig, df, bems, rows_to_display, last_uid_idx, no_assets)
                #fig.tight_layout()
                save_img(fig, figure_count)
                figsize_key = 'figsize_' + str(figure_count)
                fig_size_dict[figsize_key] = rows_to_display*html_height
            else: #If there are more figures to plot after the first page
                fig = plt.figure(figsize=(fig_width,row_height*first_page_fig_count))
                fig = plotter(fig, df, bems, first_page_fig_count, last_uid_idx, no_assets)
                #fig.tight_layout()
                save_img(fig, figure_count)
                figsize_key = 'figsize_' + str(figure_count)
                fig_size_dict[figsize_key] = first_page_fig_count*html_height
                rows_to_display = rows_to_display - first_page_fig_count
                last_uid_idx = last_uid_idx + first_page_fig_count
        elif(rows_to_display >= full_page_fig_count): #If there more bems left to plot than can fit on one page
            fig = plt.figure(figsize=(fig_width,row_height*full_page_fig_count))
            fig = plotter(fig, df, bems, full_page_fig_count, last_uid_idx, no_assets)
            #fig.tight_layout()
            save_img(fig, figure_count)
            figsize_key = 'figsize_' + str(figure_count)
            fig_size_dict[figsize_key] = full_page_fig_count*html_height
            rows_to_display = rows_to_display - full_page_fig_count
            last_uid_idx = last_uid_idx + full_page_fig_count
        else:#If on last page
            fig = plt.figure(figsize=(fig_width,row_height*rows_to_display))
            fig = plotter(fig, df, bems, rows_to_display, last_uid_idx, no_assets)
            #fig.tight_layout()
            save_img(fig, figure_count)
            figsize_key = 'figsize_' + str(figure_count)
            fig_size_dict[figsize_key] = rows_to_display*html_height
    #print(fig_size_dict)
    return fig_count,fig_size_dict

@click.command()
@click.argument('org_id')
@click.argument('email')
@click.argument('start_ts_ms')
@click.argument('end_ts_ms')
def deloader(org_id, email, start_ts_ms, end_ts_ms):
    organisation_name = list(org_collection.find({'_id':org_id}))[0]['name']
    bems,org_dict,no_assets = bems_and_assets_from_organisation(organisation_name)

    empty_batteries = 0

    start_ts_ms = int(start_ts_ms)
    end_ts_ms = int(end_ts_ms)

    start_dt = datetime.fromtimestamp(start_ts_ms/1000)
    end_dt = datetime.fromtimestamp(end_ts_ms/1000)

    start_date = datetime.strftime(start_dt.date(), '%d %b %Y')
    end_date = datetime.strftime(end_dt, '%d %b %Y')

    start_ts_ms = start_ts_ms*1.0
    end_ts_ms = end_ts_ms*1.0

    delta = (end_dt-start_dt) / timedelta(minutes=1) #Save the difference and calculate minutes between dates
    diff = int(delta)

    column_names = ['date', 'connectivity']
    bem_df = pd.DataFrame(columns = column_names)
    bem_df['date'] = (pd.date_range(start=start_dt, periods=diff,freq='min')) #Each 'date' entry is a date that is incremented by a single minute
    bem_df['date'] = bem_df['date'].map(lambda t: t.strftime('%Y-%m-%d %H:%M')) #Strip seconds from date
    bem_df.set_index('date',inplace=True,drop=True) #Set date as index

    print(str(start_date) + ': ' + str(start_ts_ms))
    print(str(end_date) + ': ' + str(end_ts_ms))
    print()

    for uid in bems:
        print(str(uid))
        #Query Mongo for this UID's minutebuckets between specified dates
        buckets = list(buckets_collection.find({'bemUID':uid,'bucketStart':{'$gte':start_ts_ms,'$lte':end_ts_ms }},
                            {'bemDate': 1, 'soc': 1, 'batteryState': 1, 'energyIn': 1, 'energyOut': 1, 'energyDelta': 1, 'cycleCounter.default.counter': 1}).sort('bucketStart', 1))
        df = pd.DataFrame(buckets)

        if df.empty:
            print("Empty df\n")
            #bems.pop(uid, 'not found')
            empty_batteries += 1
        else:
            try:
                df['batteryState'] 
            except:
                err_msg = 'No battery state recorded in minutebuckets'
                continue

            df['cycleCounter'] = pd.json_normalize(df.cycleCounter)

            min_cycle_counter = min(df['cycleCounter'])
            max_cycle_counter = max(df['cycleCounter'])

            bems[uid]['min_cycle_counter'] = min_cycle_counter
            bems[uid]['max_cycle_counter'] = max_cycle_counter

            E_max = bems[uid]['battery_capacity'] * 3600 *1000 #Calculate battery capacity in joules
            soc_b = df['soc'].iloc[0] # first element --> soc at beginning of window
            soc_f = df['soc'].iloc[-1] # last element --> soc at end of window

            #Shunt direction switches which column represents energyin and energyout
            if(bems[uid]['shunt_direction'] == 1):
                e_in = df['energyIn'].iloc[-1] - df['energyIn'].iloc[0]
                e_out = df['energyOut'].iloc[-1] - df['energyOut'].iloc[0]
            else:
                e_out = df['energyIn'].iloc[-1] - df['energyIn'].iloc[0]
                e_in = df['energyOut'].iloc[-1] - df['energyOut'].iloc[0]       

            soc_diff = soc_b-soc_f
            try:
                efficiency = (e_out - (soc_diff*E_max))/ e_in
            except:
                efficiency = 0
            if(efficiency <= 1):
                bems[uid]['efficiency'] = efficiency
            elif(np.isnan(efficiency)):
                print('Efficiency is ' + str(efficiency) + ', \n e_in = ' + str(e_in) + '\n')
                efficiency = 1
            else:
                print('Efficiency calculation error! Efficienccy is higher than 100%: ' + str(efficiency*100) + '\n')

            #Remove duplicate and NAN values, set time as index
            df.drop('_id', inplace=True, axis=1)
            df = df[df['soc'].notna()] #Drop NAN values from buckets df
            df.set_index('bemDate', inplace=True, drop=True) #Set date as index
            df.index = pd.to_datetime(df.index) #Convert index to datetime 
            df.index =df.index.map(lambda t: t.strftime('%Y-%m-%d %H:%M')) #Strip seconds from dates
            df = df[~df.index.duplicated()] #Remove duplicate entries

            #Create state of charge rate of change column: Difference in SOC from one minute to next
            df['socroc'] = df['soc'].pct_change(1) 

            #Remove all NAN values from new column
            df = df[df['socroc'].notna()] 
            df = df.replace(np.NINF, np.NAN)
            df = df.replace(np.inf, np.NAN)
            df.dropna(inplace=True)

            threshold = 0.0009 #Battery at rest(idle) will not have a soc roc larger than |0.0009|
            charging_conditional = df['batteryState'] == 'Charging' #All buckets in charging mode
            not_charging_conditional = df['batteryState'] != 'Charging' #All buckets not in charging mode (in use or idle)
            inside_thresh = (df['socroc'] > -threshold) & (df['socroc'] < threshold) #All buckets in idle
            lt_thresh = df['socroc'] <= -threshold #All buckets in use
            gt_thresh = df['socroc'] >= threshold #All buckets in charge

            df.loc[lt_thresh & not_charging_conditional, 'batteryState'] = 'In Use'
            df.loc[gt_thresh | charging_conditional, 'batteryState'] = 'Charging'
            df.loc[inside_thresh, 'batteryState'] = 'Idle'

            df['newstate'] = ''
            
            bucket_list = df[['batteryState','newstate']].values.tolist()
            list_len = len(bucket_list)
            #The following functionality could not be accomplished using pandas vectorisation, so it is converted to list
            for i in range(list_len):

                from_end = list_len-i
                #This loop tries to catch "idle" states nestled between "in use" states. Only 5 sequential "idle"...
                #... states will result in "idle". Otherwise t
                #From_end ensures that index out of range is not referenced in subsequent condition checks
                #The first if condition also checks whether newState has already been assigned an "idle" state...
                #...this ensures the same buckets are not revisited each iteration of the for loop
                if( (from_end>1) and (bucket_list[i][0] == 'Idle') and (bucket_list[i][1] != 'Idle') ):
                    if((from_end>2) and (bucket_list[i+1][0] == 'Idle')):
                        if((from_end>3) and (bucket_list[i+2][0] == 'Idle')):
                            if((from_end>4) and (bucket_list[i+3][0] == 'Idle')):
                                if((from_end>5) and (bucket_list[i+4][0] == 'Idle')):
                                    if((from_end>6) and (bucket_list[i+5][0] == 'Idle')):
                                        bucket_list[i][1] = 'Idle'
                                        bucket_list[i+1][1] = 'Idle'
                                        bucket_list[i+2][1] = 'Idle'
                                        bucket_list[i+3][1] = 'Idle'
                                        bucket_list[i+4][1] = 'Idle'
                                    else:
                                        bucket_list[i][1] = 'In Use'
                                        bucket_list[i+1][1] = 'In Use'
                                        bucket_list[i+2][1] = 'In Use'
                                        bucket_list[i+3][1] = 'In Use'
                                        bucket_list[i+4][1] = 'In Use'
                                else:
                                    bucket_list[i][1] = 'In Use'
                                    bucket_list[i+1][1] = 'In Use'
                                    bucket_list[i+2][1] = 'In Use'
                                    bucket_list[i+3][1] = 'In Use'
                            else:
                                bucket_list[i][1] = 'In Use'
                                bucket_list[i+1][1] = 'In Use'
                                bucket_list[i+2][1] = 'In Use'
                        else:
                            bucket_list[i][1] = 'In Use'
                            bucket_list[i+1][1] = 'In Use'
                    else:
                        bucket_list[i][1] = 'In Use'

            battery_state_series = pd.Series( (v[0] for v in bucket_list) )
            battery_state_series.index = df.index

            new_state_series = pd.Series( (v[1] for v in bucket_list) )
            new_state_series.index = df.index

            df['batteryState'] = battery_state_series
            df['newstate'] = new_state_series

            use_idx = df['batteryState'] == 'In Use'
            chg_idx = df['batteryState'] == 'Charging'
            df.loc[use_idx, 'newstate'] = 'In Use'
            df.loc[chg_idx, 'newstate'] = 'Charging'

            df['kJ'] = (df['energyDelta'] - df['energyDelta'].shift())/1000 #Total energy used in the minute
            df['kW'] = df['kJ']/60 #Energy per second -->  60 seconds in a minute
            df['kWh'] = df['kW']/60 #Basically multiply by the hours the work was done for which is one minute (1/60)

            work_rate = rate_calc(df,'In Use')
            work_rate += rate_calc(df,'Idle')
            charge_rate = rate_calc(df,'Charging')

            bems[uid]['workrate'] = work_rate*bems[uid]['shunt_direction']
            bems[uid]['charge_rate'] = charge_rate

            new_df = df.drop(df.columns.difference(['newstate','soc']), inplace=True, axis=1)
            new_df = df.replace(np.NINF, np.NAN)
            new_df = df.replace(np.inf, np.NAN)
            new_df.dropna(inplace=True)
            bem_df[uid] = new_df['newstate']

    bem_df.drop('connectivity',inplace=True,axis=1)
    df = bem_df





    #DATA VALIDATION#############################################################################
    incomplete_data = 0
    bem_count = 0
    avg_workrate = 0
    avg_cycles = 0
    avg_idle = 0
    avg_use = 0
    avg_chg = 0
    for uid in df:
        #Calculate averages
        avg_cycles += (bems[uid]['max_cycle_counter'] - bems[uid]['min_cycle_counter'])
        avg_workrate += bems[uid]['workrate']

        try:
            avg_idle += df[uid].value_counts()['Idle']
        except:
            avg_idle += 0
        try:
            avg_use += df[uid].value_counts()['In Use']
        except:
            avg_use += 0
        try:
            avg_chg += df[uid].value_counts()['Charging']
        except:
            avg_chg += 0

        #Calculate fraction of entries in this column that are NAN
        col = df[uid]

        nan_fraction = int(((col.count()- col.isna().sum())/col.count())*100) 
        #print(uid + str(': ' + str(nan_fraction)))
        if(nan_fraction < 50):
            df.drop(uid, inplace=True, axis=1)
            #bems.pop(uid,'not found')
            incomplete_data += 1
            print(uid + ' has no or less than 50% of data present \n')
            continue
        print(uid)
        bem_count += 1
        new = (col.value_counts()/60).astype(int)
        for state in ['Idle','In Use','Charging']:
            try:
                new[state]
            except:
                print('No ' + state + ' state present \n')



    #GENERATE PLOTS ###########################################################################
    fig_count,fig_size_dict = figure_generator(df, bems, no_assets, bem_count)




    #OVERVIEW PIE CHART #########################################################################
    colours = {'In Use': color('In Use'),'Idle': color('Idle'),'Charging': color('Charging')}
    explode = (0.02, 0.02, 0.02)

    avg_cycles = avg_trycatcher(avg_cycles,bem_count,True)#round(avg_cycles/bem_count,1)
    avg_workrate = avg_trycatcher(avg_workrate,bem_count,True)
    avg_idle = avg_trycatcher(avg_idle,bem_count,False)
    avg_use = avg_trycatcher(avg_use,bem_count,False)
    avg_chg = avg_trycatcher(avg_chg,bem_count,False)

    overview_fig,ox = plt.subplots(1,1,figsize=(8,8))

    pie_labels = ['Idle', 'In Use', 'Charging']

    values = [avg_idle,avg_use,avg_chg]
    ox.pie(
        values, 
        labels = ['Idle','In Use','Charging'],
        labeldistance=None,
        autopct=make_autopct(values),
        radius=1,
        textprops={'color':'0','fontsize': 22},
        colors=[colours[key] for key in pie_labels],
        wedgeprops={"edgecolor":color('font'),'linewidth': 1, 'linestyle': 'solid', 'antialiased': True}, 
        startangle=90,explode=explode)

    overview_fig.savefig('media/overview_pie.svg',bbox_inches='tight')  
    




    #GENERATE PDF #############################################
    #Customer name font size dictated by string length
    customer_name_font_size = 32
    costomer_name_len = len(org_dict['organisation'])
    if(costomer_name_len > 45):
        customer_name_font_size = 22
    elif(costomer_name_len > 32):
        customer_name_font_size = 25

    env = Environment(loader=FileSystemLoader('.'))
    template = env.get_template("media/grouped_template.html")

    template_vars = {'org_name': org_dict['organisation'],
                        'org_branding':org_dict['org_branding'],
                        
                        'start_date': start_date,
                        'end_date': end_date,
                        
                        'bem_count':bem_count,
                        'empty_batteries': empty_batteries,
                        'incomplete_batteries':incomplete_data,
                        
                        'avg_workrate':abs(avg_workrate),
                        'avg_cycles': int(avg_cycles),
                        
                        'customer_name_font_size':customer_name_font_size,
                        'fig_count':fig_count}

    template_vars.update(fig_size_dict)

    html_out = template.render(template_vars) #template_vars

    with open('media/grouped_report.html', 'w') as f:
        f.write(html_out)

    pdfname = "report.pdf"
    pdfkit.from_file('media/grouped_report.html', pdfname)





    #EMAILER##############################################################################################
    rep_str = start_date + str('  -  ') + end_date
    email_body = 'Good day, \nPlease find attached your requested report for ' + rep_str + '\nKind regards \nBalancell'
    
    sender = 'kahlkritzinger@balancell.co'
    password = 'Will2211'
    receiver = email

    #Setup the MIME
    message = MIMEMultipart()
    message['From'] = sender
    message['To'] = receiver
    message['Subject'] = 'Report for ' + rep_str

    message.attach(MIMEText(email_body, 'plain'))
    

    # open the file in bynary
    binary_pdf = open(pdfname, 'rb')

    payload = MIMEBase('application', 'octate-stream', Name=pdfname)
    payload.set_payload((binary_pdf).read())

    # enconding the binary into base64
    encoders.encode_base64(payload)

    # add header with pdf name
    payload.add_header('Content-Decomposition', 'attachment', filename=pdfname)
    message.attach(payload)

    #use gmail with port
    session = smtplib.SMTP('smtp.gmail.com', 587)

    #enable security
    session.starttls()

    #login with mail_id and password
    session.login(sender, password)

    text = message.as_string()
    session.sendmail(sender, receiver, text)
    session.quit()
    print('Mail Sent')
if __name__ == '__main__':
    deloader()