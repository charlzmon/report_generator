#!/bin/bash

echo Executing grouped report generator with organisation:$1, email:$2, start_ts:$3, end_ts:$4

#source ~/.venvs/repgen/bin/activate

cd organisation_batteries
python grouped_batteries.py "$1" "$2" "$3" "$4"
